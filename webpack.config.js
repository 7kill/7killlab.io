/**
 * Just for IDE path resolve
 * This config is not required for app functionality
 */
module.exports = {
    resolve: {
        // for WebStorm
        alias: {
            '@': path.resolve(__dirname),
            '~': path.resolve(__dirname)
        }
    }
}
